/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxgame;

import java.util.*;

/**
 *
 * @author dell
 */
public class OxGame {

    /**
     * @param args the command line arguments
     */
    public static void showWelcome() {
        System.out.println("Welcome to OX Game!");
    }

    public static void showTable(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void showTurn(char player) {
        System.out.println(player + " turn :");
    }

    public static void inputRowCol(char[][] table, char player) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input Row Col >> ");
        try {
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;
            if (row < 0 || row > 2 || col < 0 || col > 2) {
                showWrongPosition();
                inputRowCol(table, player);
            } else {
                inputOX(table, player, row, col);
            }
        } catch (Exception e) {
            showInputOnlyNumber();
            kb.nextLine();
            inputRowCol(table, player);
        }
    }

    public static void inputOX(char[][] table, char player, int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = player;
        } else {
            showNotAvailable();
            inputRowCol(table, player);
        }
    }

    public static void showNotAvailable() {
        System.out.println("This position is not available!!!");
    }

    public static void showWrongPosition() {
        System.out.println("This position is wrong!!!");
    }

    public static void showInputOnlyNumber() {
        System.out.println("Input only integer numbers!!!");
    }

    public static char switchPlayer(char player) {
        if (player == 'X') {
            return 'O';
        } else {
            return 'X';
        }
    }

    public static boolean checkWin(char[][] table) {
        if (checkWinRow(table) || checkWinCol(table) || checkWinDiagonal(table)) {
            return true;
        }
        return false;
    }

    public static boolean checkWinRow(char[][] table) {
        for (int row = 0; row < table.length; row++) {
            if (table[row][0] == table[row][1] && table[row][0] == table[row][2]
                    && table[row][0] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkWinCol(char[][] table) {
        for (int col = 0; col < table.length; col++) {
            if (table[0][col] == table[1][col] && table[0][col] == table[2][col]
                    && table[0][col] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkWinDiagonal(char[][] table) {
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            return true;
        } else if (table[0][2] == table[1][1] && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            return true;
        }
        return false;
    }

    public static boolean checkDraw(char[][] table) {
        int sum = 0;
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table[i][j] != '-') {
                    sum++;
                }
            }
        }
        if (sum == 9 && !checkWin(table)) {
            return true;
        }
        return false;
    }

    public static void showGoodBye() {
        System.out.println("Good Bye...");
    }

    public static void showWin(char winner) {
        System.out.println("----" + winner + " Win!!!----");
    }

    public static void showDraw() {
        System.out.println("----Draw!!!----");
    }

    public static void main(String[] args) {
        char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char player = 'O';
        char winner;

        showWelcome();
        showTable(table);
        while (true) {
            showTurn(player);
            inputRowCol(table, player);
            showTable(table);
            if (checkWin(table)) {
                winner = player;
                showWin(winner);
                break;
            }
            if (checkDraw(table)) {
                showDraw();
                break;
            }
            player = switchPlayer(player);
        }
        showGoodBye();
    }
}
